package fr.kazix.bdd;

import java.sql.*;

public class BddManager {
    public static final BddManager INSTANCE = new BddManager();

    String bddName;
    String bddUrl;
    String bddUserName;
    String bddUserPassword;

    Connection conn;

    private BddManager(){

    }

    //Setup -> renseigner les infos de connexion
    public void setup(String bddName, String bddUrl, String bddUserName, String bddUserPassword){
        this.bddName = bddName;
        this.bddUrl = bddUrl;
        this.bddUserName = bddUserName;
        this.bddUserPassword = bddUserPassword;
    }

    //login
    public void login() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(bddUrl.concat(bddName), bddUserName, bddUserPassword);
    }


    //Deconnecter
    public void logout() throws SQLException {
        if(conn != null){
            conn.close();
        }
    }


    //Executer une requête
    public ResultSet executeQuery(boolean isUpdate, String query, Object... parameters) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(query);
        for(int i = 1; i <= parameters.length; i++) {
            statement.setString(i, parameters[i-1].toString());
        }
        if(isUpdate)
        {
            statement.executeUpdate();
            return null;
        }
        return statement.executeQuery();
    }

}
