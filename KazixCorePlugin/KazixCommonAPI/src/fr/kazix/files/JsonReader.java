package fr.kazix.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class JsonReader {


    public <T> T read(Class<T> theClass, String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileName), UTF_8);
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line);
        }

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(sb.toString(), theClass);
    }
}
