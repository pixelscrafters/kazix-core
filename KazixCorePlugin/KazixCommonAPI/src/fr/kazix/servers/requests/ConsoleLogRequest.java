package fr.kazix.servers.requests;

import fr.kazix.discord.IDiscordListener;

public class ConsoleLogRequest {
    public String loggerId = "";
    public String message = "";
    public IDiscordListener.LogLevel level = IDiscordListener.LogLevel.NORMAL;
}
