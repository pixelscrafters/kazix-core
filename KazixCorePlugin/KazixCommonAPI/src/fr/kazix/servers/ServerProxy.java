package fr.kazix.servers;

import fr.kazix.Utils;
import fr.kazix.servers.requests.AuthRequest;
import fr.kazix.servers.requests.ConsoleLogRequest;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ServerProxy extends Thread {
    private String serverName;
    private Socket socket;
    private boolean stopRequired = false;
    private PrintWriter out;

    ServerProxy(String serverName, Socket socket) {
        this.serverName = serverName;
        this.socket = socket;
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();
        while (!stopRequired) {
            try {
                if (out == null) {
                    out = new PrintWriter(socket.getOutputStream(), true);
                }
                InputStream is = socket.getInputStream();
                Scanner scanner = new Scanner(is, "UTF-8");
                String line = scanner.nextLine();
                MessageProxy messageProxy = MessageProxy.fromJson(line);
                if (messageProxy.getParameterType() == AuthRequest.class) {
                    this.serverName = ((AuthRequest) messageProxy.getParameter()).serverName;
                    ServerManager.INSTANCE.authByServerProxy(this);
                } else if (messageProxy.getParameterType() == ConsoleLogRequest.class) {
                    ConsoleLogRequest consoleLogRequest = (ConsoleLogRequest) messageProxy.getParameter();
                    Utils.discordListener.onServerLog(consoleLogRequest.level, consoleLogRequest.loggerId, consoleLogRequest.message);
                }
            } catch (IOException | ClassNotFoundException ex) {
                stopRequired = false;
            } catch (NoSuchElementException | NullPointerException ex) {
                stopRequired = false;
            }
        }
    }

    public String getServerName() {
        return serverName;
    }

    private synchronized void requestSync(MessageProxy cr) {
        out.println(cr.toString());
    }

    public void request(MessageProxy cr) {
        new Thread(() -> requestSync(cr)).start();
    }

}
