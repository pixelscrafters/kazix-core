package fr.kazix.servers.client;

import fr.kazix.servers.MessageProxy;
import fr.kazix.servers.requests.AuthRequest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientServer {
    protected Socket socket;
    private PrintWriter out;
    private ClientServerReader scr;
    private INetworkEventManager networkEventManager;

    public INetworkEventManager getNetworkEventManager() {
        return networkEventManager;
    }

    public void connect(String serverName, INetworkEventManager networkEventManager) {
        this.networkEventManager = networkEventManager;
        //TODO configurable serverName
        try {
            socket = new Socket("127.0.0.1", 65200);
            out = new PrintWriter(socket.getOutputStream(), true);
            Scanner in = new Scanner(new InputStreamReader(socket.getInputStream()));
            //READER
            scr = new ClientServerReader(in, this);
            scr.start();
            //Starting authentification
            AuthRequest authRequest = new AuthRequest();
            authRequest.serverName = serverName;

            request(new MessageProxy(authRequest, AuthRequest.class));
            System.out.println("Sended request.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void requestSync(MessageProxy cr) {
        out.println(cr.toString());
    }

    public void request(MessageProxy cr) {
        new Thread(() -> requestSync(cr)).start();
    }
}
