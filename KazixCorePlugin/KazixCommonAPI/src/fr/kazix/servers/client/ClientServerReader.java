package fr.kazix.servers.client;

import fr.kazix.dependencies.pixelsskyblock.AbstractSkyblockConfigurationElement;
import fr.kazix.dependencies.pixelsskyblock.SkyblockManager;
import fr.kazix.servers.MessageProxy;
import fr.kazix.servers.requests.ConsoleCommandRequest;

import java.util.Scanner;

public class ClientServerReader extends Thread {
    Scanner reader;
    ClientServer client;
    boolean stopRequired = false;

    ClientServerReader(Scanner reader, ClientServer client) {
        this.reader = reader;
        this.client = client;
    }

    @Override
    public void run() {
        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            //TODO receive messages from bungee here
            System.out.println(line);
            MessageProxy messageProxy = MessageProxy.fromJson(line);
            try {
                if (messageProxy.getParameterType() == ConsoleCommandRequest.class) {
                    client.getNetworkEventManager().onCommandReceived(((ConsoleCommandRequest) messageProxy.getParameter()).command);
                } else if(messageProxy.getParameterType() == AbstractSkyblockConfigurationElement.class){
                    try {
                        SkyblockManager.dispatch((AbstractSkyblockConfigurationElement) messageProxy.getParameter());
                    }catch (NullPointerException ex){
                        //TODO LOG ERROR
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
