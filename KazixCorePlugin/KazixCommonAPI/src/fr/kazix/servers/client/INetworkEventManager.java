package fr.kazix.servers.client;

public interface INetworkEventManager {
    public void onCommandReceived(String command);
}
