package fr.kazix.servers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

public class MessageProxy {

    public String data;
    private String parameterType;

    public MessageProxy() {

    }

    ;

    public MessageProxy(Object parameter, Type parameterType) {
        Gson gson = new GsonBuilder().create();
        this.data = gson.toJson(parameter);
        this.parameterType = parameterType.getTypeName();
    }

    public static MessageProxy fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, MessageProxy.class);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public Object getParameter() throws ClassNotFoundException {
        Gson gson = new GsonBuilder().create();
        Class<?> theClass = Class.forName(parameterType);
        return gson.fromJson(data, theClass);
    }

    public Type getParameterType() throws ClassNotFoundException {
        return Class.forName(parameterType);
    }
}
