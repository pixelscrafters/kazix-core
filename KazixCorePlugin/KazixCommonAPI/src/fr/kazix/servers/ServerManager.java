package fr.kazix.servers;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.TreeMap;

public class ServerManager extends Thread {
    public static final ServerManager INSTANCE = new ServerManager();

    private ServerSocket socket;

    private TreeMap<String, ServerProxy> servers = new TreeMap<>();
    private boolean stopRequired = false;

    private ServerManager() {

    }

    public void bind(int port) {
        try {
            socket = new ServerSocket();
            socket.bind(new InetSocketAddress(port));
            this.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();
        while (!stopRequired) {
            try {
                Socket newClient = socket.accept();
                ServerProxy sp = new ServerProxy("NOT_IDENTIFIED", newClient);
                sp.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean authByServerProxy(ServerProxy serverProxy) {
        if (isAuthentified(serverProxy.getServerName())) {
            return false;
        }
        servers.put(serverProxy.getServerName(), serverProxy);
        System.out.println("Authentified server : " + serverProxy.getServerName());
        return true;
    }

    public boolean isAuthentified(String serverName) {
        if (servers.containsKey(serverName)) {
            return true;
        }
        return false;
    }

    public void remove(String serverName) {
        try {
            servers.remove(serverName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ServerProxy findServerProxy(String serverName) {
        ServerProxy serverProxy = servers.get(serverName);
        if (serverProxy == null) {
            //TODO : Send warning.
            return null;
        }
        return serverProxy;
    }


}
