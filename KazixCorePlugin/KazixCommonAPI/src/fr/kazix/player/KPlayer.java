package fr.kazix.player;

/**
 * Class used in order to manage player data.
 */
public class KPlayer {
    private String uuid;
    private String name;
    private String rank = "NOTHING";
    private int level = 0;

    public KPlayer(String uuid, String name, String rank, int level) {
        this.uuid = uuid;
        this.name = name;
        this.rank = rank;
        this.level = level;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getRank() {
        return rank;
    }

    public int getLevel() {
        return level;
    }

    public String formatChat(String message, String[] insults) {
        for (String s : insults) {
            message = message.replaceAll("(?i)" + s, "§lJE T'AIME <3§d");
        }
        return "§d-> §5[" + rank + "] §d" + level + " §7" + name + " §d : " + message;
    }
}
