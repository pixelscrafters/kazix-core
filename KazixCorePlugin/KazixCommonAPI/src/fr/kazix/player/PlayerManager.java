package fr.kazix.player;

import java.util.TreeMap;

public class PlayerManager {
    //WARNING DO NOT USE IN BUKKIT
    //TODO MUTEX

    private static TreeMap<String, KPlayer> players = new TreeMap<>();

    public KPlayer getPlayer(String uuid) {
        return players.get(uuid);
    }

    public KPlayer load(String uuid) {
        //TODO : Chargement des données depuis BDD
        return null;
    }

    public void save() {
        //TODO : sauvegarde des données vers BDD
    }

    public void addNewPlayer(KPlayer player) {
        if (!players.containsKey(player.getUuid())) {
            this.players.put(player.getUuid(), player);
        }
    }
}
