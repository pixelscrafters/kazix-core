package fr.kazix.mapping;

import fr.kazix.mapping.terrain.Space;

import java.util.List;

public abstract class AbstractTerrainObject {
    public Space space;

    /**
     * Used to know if its broken or not.
     */
    public int health;

    /**
     * Used in order to do things when in use.
     */
    public int energy;

    /**
     * Objects placed INSIDE this object. Note : Space has to be relative.
     */
    public List<Object> subObjects;

    public int level;
    public int maxLevel;

    public List<Object> playerRequirments;

    /**
     * Constructor
     *
     * @param space
     */
    public AbstractTerrainObject(Space space, int level, int maxLevel) {
        this.space = space;
        this.level = level;
        this.maxLevel = maxLevel;
    }

    /**
     * Used in order to draw
     *
     * @param level
     */
    public abstract void place(int level);

    public abstract void remove();

    public abstract void destroy();

    public abstract void upgrade();

    public abstract void onPlayerUse();

    public abstract void onDamaged();

}
