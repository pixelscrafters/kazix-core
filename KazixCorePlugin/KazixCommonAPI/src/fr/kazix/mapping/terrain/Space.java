package fr.kazix.mapping.terrain;

/**
 * Used in order to place buildings, roads, ...
 */
public class Space {
    /**
     * Orientation of the placed object.
     */
    public Orientation orientation;
    /**
     * Location of the placed object.
     */
    public int x;
    public int y;
    public int z;
    /**
     * Size of the placed object.
     */
    public int width;
    public int height;
    public enum Orientation {
        NORT, SOUTH, EAST, WEST
    }
}
