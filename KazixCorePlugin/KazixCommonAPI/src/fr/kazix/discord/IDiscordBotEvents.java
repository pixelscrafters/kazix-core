package fr.kazix.discord;

public interface IDiscordBotEvents {
    public void onChatMessageReceived(String message);

    public void onCommandReceived(String serverName, String command);
}
