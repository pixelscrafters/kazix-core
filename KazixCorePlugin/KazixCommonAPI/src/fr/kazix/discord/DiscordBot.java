package fr.kazix.discord;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;
import java.text.SimpleDateFormat;
import java.util.TreeMap;

public class DiscordBot extends ListenerAdapter implements IDiscordListener {
    public IDiscordBotEvents registeredListener;
    private DiscordConfiguration configuration;
    private JDA jda;
    private TextChannel adminNotifications;
    private TreeMap<String, TextChannel> consoles = new TreeMap<>();
    private TextChannel chatChannel;
    private int playerCount = 0;

    //NDY0MTYzODYzMTgxNTI0OTk0.XfFAmQ.jp40M_I6b6T7GM9_nikaLzfNneU
    public void createBot(DiscordConfiguration configuration) throws LoginException, InterruptedException {
        this.configuration = configuration;
        //Load token from configuration;
        JDABuilder builder = new JDABuilder(AccountType.BOT);
        builder.setToken(configuration.token);
        builder.addEventListener(this);
        //Set format...
        builder.setGame(Game.playing(configuration.statusFormat.replace("{players}", "0")));
        jda = builder.buildBlocking();
        try {
            chatChannel = jda.getTextChannelById(configuration.chatChannelId);
        } catch (Exception ex) {

        }
        try {
            adminNotifications = jda.getTextChannelById(configuration.adminInfoChannelId);
        } catch (Exception ex) {

        }

        try {
            for (String chanName : configuration.consoleChannelsId.keySet()) {
                consoles.put(chanName, jda.getTextChannelById(configuration.consoleChannelsId.get(chanName)));
            }
        } catch (Exception ex) {

        }
    }


    @Override
    public void onMessageSent(String text) {
        if (chatChannel != null) {
            chatChannel.sendMessage(text).queue();
        }
    }

    @Override
    public void onPlayerJoin(String playerName) {
        if (chatChannel != null) {
            chatChannel.sendMessage(":green_circle: " + playerName + " s'est connecté.").queue();
        }
        playerCount++;
        jda.getPresence().setGame(Game.playing(configuration.statusFormat.replace("{players}", "" + playerCount)));
    }

    @Override
    public void onPlayerLeft(String playerName) {
        if (chatChannel != null) {
            chatChannel.sendMessage(":red_circle: " + playerName + " s'est déconnecté.").queue();
        }
        playerCount--;
        jda.getPresence().setGame(Game.playing(configuration.statusFormat.replace("{players}", "" + playerCount)));
    }

    @Override
    public void onServerLog(LogLevel level, String identifier, String message) {
        try {
            consoles.get(identifier).sendMessage(level.format(message)).queue();
        } catch (Exception ex) {

        }
    }

    @Override
    public void onAdminNotification(LogLevel level, String message) {
        if (adminNotifications != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("[dd-MM-yyyy hh:mm:ss] ");
            adminNotifications.sendMessage(sdf.format(System.currentTimeMillis()) + message).queue();
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (registeredListener == null) {
            return;
        }

        User author = event.getAuthor();                //The user that sent the message
        Message message = event.getMessage();           //The message that was received.
        TextChannel channel = event.getTextChannel();

        boolean bot = author.isBot();

        if (!bot)         //If this message was sent to a Guild TextChannel
        {
            if (channel.getId().equals(adminNotifications.getId())) {
                onAdminNotification(LogLevel.ERROR, "Vous ne pouvez pas envoyer de commandes ici.");
            } else if (channel.getId().equals(chatChannel.getId())) {
                registeredListener.onChatMessageReceived(author.getName() + " " + message.getContentDisplay());
            } else {
                for (String s : consoles.keySet()) {
                    TextChannel console = consoles.get(s);
                    if (channel.getId().equals(console.getId())) {
                        onAdminNotification(LogLevel.NORMAL, "Received command for " + s + " : " + message.getContentDisplay());
                        registeredListener.onCommandReceived(s, message.getContentDisplay());
                    }
                }
            }
        }

        super.onMessageReceived(event);
    }
}
