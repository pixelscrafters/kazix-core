package fr.kazix.discord;

public interface IDiscordListener {
    public void onMessageSent(String text);

    public void onPlayerJoin(String playerName);

    public void onPlayerLeft(String playerName);

    public void onServerLog(LogLevel level, String identifier, String message);

    public void onAdminNotification(LogLevel level, String message);

    public enum LogLevel {
        NORMAL {
            @Override
            public String format(String s) {
                return "```css\n" +
                        "." + s + "\n" +
                        "```";
            }
        },
        WARNING {
            @Override
            public String format(String s) {
                return "```fix\n" +
                        s + "\n" +
                        "```";
            }
        },
        ERROR {
            @Override
            public String format(String s) {
                return  "```diff\n" +
                        "- " + s +
                        "```";
            }
        };

        public abstract String format(String s);
    }
}
