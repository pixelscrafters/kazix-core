package fr.kazix.discord;

import java.util.TreeMap;

public class DiscordConfiguration {
    ///Partie config connexion
    public String token = "";

    //Partie channels
    public String chatChannelId = "";
    public String adminInfoChannelId = "";
    public TreeMap<String, String> consoleChannelsId = new TreeMap<>();
    //public String consoleChannelId = "";

    //Partie texte
    public String prefix = "";
    public String chatPrefix = "";
    public String adminInfoPrefix = "";
    public String consolePrefix = "";

    //Partie status...
    public String statusFormat = "{players} Joueurs.";

}
