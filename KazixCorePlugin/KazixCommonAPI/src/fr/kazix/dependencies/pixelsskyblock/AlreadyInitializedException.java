package fr.kazix.dependencies.pixelsskyblock;

public class AlreadyInitializedException extends Exception {
    private String message = "";

    public AlreadyInitializedException(String s){
        message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
