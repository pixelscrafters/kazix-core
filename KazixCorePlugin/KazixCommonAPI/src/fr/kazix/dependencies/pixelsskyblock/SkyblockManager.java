package fr.kazix.dependencies.pixelsskyblock;


public class SkyblockManager {
    private static ISkyblockConfigurator registeredEventReceiver;

    public static void registerSkyblockConfigurator(ISkyblockConfigurator configurator) throws AlreadyInitializedException {
        if(registeredEventReceiver != null){
            throw new AlreadyInitializedException("Skyblock configurator already set");
        }
        registeredEventReceiver = configurator;
    }

    public static void dispatch(AbstractSkyblockConfigurationElement configurationElement){
        registeredEventReceiver.onReceive(configurationElement);
    }

}
