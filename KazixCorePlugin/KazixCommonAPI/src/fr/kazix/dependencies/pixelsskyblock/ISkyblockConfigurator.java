package fr.kazix.dependencies.pixelsskyblock;

public interface ISkyblockConfigurator {
    void onReceive(AbstractSkyblockConfigurationElement configurationElement);
}
