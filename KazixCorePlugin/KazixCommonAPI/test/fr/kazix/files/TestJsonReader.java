package fr.kazix.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.kazix.discord.DiscordConfiguration;
import org.junit.Test;

import java.io.IOException;

public class TestJsonReader {
    @Test
    public void testJsonReader() throws IOException {
        Gson gson = new GsonBuilder().create();
        DiscordConfiguration temp = new DiscordConfiguration();
        temp.consoleChannelsId.put("aa", "bb");
        String s = gson.toJson(temp);
        System.out.println("1 : " + s);

        DiscordConfiguration configuration = new DiscordConfiguration();
        JsonReader configReader = new JsonReader();
        configuration = configReader.read(DiscordConfiguration.class, "discord.json");

        gson = new GsonBuilder().create();
        String s2 = gson.toJson(configuration);
        System.out.println("\n\n2 : " + s2);
    }
}
