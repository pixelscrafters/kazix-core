package fr.kazix.bukkit;

import fr.kazix.bukkit.listeners.PlayerListener;
import fr.kazix.bukkit.logs.LoggerAppender;
import fr.kazix.files.JsonReader;
import fr.kazix.servers.ServerConfiguration;
import fr.kazix.servers.client.ClientServer;
import org.apache.logging.log4j.LogManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Random;

public class BukkitMain extends JavaPlugin {
    private static final org.apache.logging.log4j.core.Logger logger = (org.apache.logging.log4j.core.Logger) LogManager.getRootLogger();
    private static BukkitMain instance = null;
    private static String serverName = "IKLOURH";

    public static String getServerName() {
        return serverName;
    }

    public static BukkitMain getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        JsonReader jsonReader = new JsonReader();
        try {
            System.out.println("Reading server identifier ...");
            ServerConfiguration configuration = jsonReader.read(ServerConfiguration.class, "server_configuration.json");
            serverName = configuration.serverName;
            System.out.println("Server identifier : " + serverName);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        instance = this;

        ClientServer clientServer = new ClientServer();
        clientServer.connect(serverName, new NetworkListener());
        LoggerAppender appender = new LoggerAppender(clientServer);

        logger.addAppender(appender);

        //Listener
        this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
