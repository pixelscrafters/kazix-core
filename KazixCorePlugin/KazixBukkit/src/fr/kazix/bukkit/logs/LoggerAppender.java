package fr.kazix.bukkit.logs;

import fr.kazix.bukkit.BukkitMain;
import fr.kazix.discord.IDiscordListener;
import fr.kazix.servers.MessageProxy;
import fr.kazix.servers.client.ClientServer;
import fr.kazix.servers.requests.ConsoleLogRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
// other imports that you need here

public class LoggerAppender extends AbstractAppender {
    ClientServer server;
    // your variables

    public LoggerAppender(ClientServer server) {
        super("MyLogAppender", null, null);

        this.server = server;
        start();
    }

    @Override
    public void append(LogEvent event) {
        LogEvent log = event.toImmutable();

        Level l = log.getLevel();
        IDiscordListener.LogLevel level = IDiscordListener.LogLevel.NORMAL;

        if (l == Level.INFO) {
            level = IDiscordListener.LogLevel.NORMAL;
        } else if (l == Level.WARN) {
            level = IDiscordListener.LogLevel.WARNING;
        } else if (l == Level.ERROR) {
            level = IDiscordListener.LogLevel.ERROR;
        }

        ConsoleLogRequest consoleLogRequest = new ConsoleLogRequest();
        consoleLogRequest.loggerId = BukkitMain.getServerName();
        consoleLogRequest.message = log.getMessage().getFormattedMessage();
        consoleLogRequest.level = level;

        server.request(new MessageProxy(consoleLogRequest, ConsoleLogRequest.class));
    }

}
