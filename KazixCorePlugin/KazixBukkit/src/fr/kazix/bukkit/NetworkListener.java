package fr.kazix.bukkit;

import fr.kazix.servers.client.INetworkEventManager;

public class NetworkListener implements INetworkEventManager {
    @Override
    public void onCommandReceived(String command) {
        BukkitMain.getInstance().getServer().getScheduler().runTask(BukkitMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                BukkitMain.getInstance().getServer()
                        .dispatchCommand(BukkitMain.getInstance().getServer().getConsoleSender(), command);
            }
        });
    }
}
