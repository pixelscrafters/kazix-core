package fr.kazix.bungee.logs;

import fr.kazix.discord.DiscordBot;
import fr.kazix.discord.IDiscordListener;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
// other imports that you need here

public class LoggerAppender extends AbstractAppender {
    DiscordBot linkedBot;
    // your variables

    public LoggerAppender(DiscordBot linkedBot) {
        super("MyLogAppender", null, null);

        this.linkedBot = linkedBot;
        start();
    }

    @Override
    public void append(LogEvent event) {
        // if you don`t make it immutable, than you may have some unexpected behaviours
        LogEvent log = event.toImmutable();
        // do what you have to do with the log

        // you can get only the log message like this:
        Level l = log.getLevel();
        IDiscordListener.LogLevel level = IDiscordListener.LogLevel.NORMAL;
        if (l == Level.INFO) {
            level = IDiscordListener.LogLevel.NORMAL;
        } else if (l == Level.WARN) {
            level = IDiscordListener.LogLevel.WARNING;
        } else if (l == Level.ERROR) {
            level = IDiscordListener.LogLevel.ERROR;
        }
        linkedBot.onServerLog(level, "bungee", log.getMessage().getFormattedMessage());
    }

}
