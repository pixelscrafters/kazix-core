package fr.kazix.bungee.logs;

import fr.kazix.bungee.BungeeMain;
import fr.kazix.discord.IDiscordBotEvents;
import fr.kazix.discord.IDiscordListener;
import fr.kazix.servers.MessageProxy;
import fr.kazix.servers.ServerManager;
import fr.kazix.servers.requests.ConsoleCommandRequest;
import net.md_5.bungee.api.chat.TextComponent;

public class DiscordMessagesReceiver implements IDiscordBotEvents {
    @Override
    public void onChatMessageReceived(String message) {
        BungeeMain.getInstance().getProxy().broadcast(new TextComponent(message));
    }

    @Override
    public void onCommandReceived(String serverName, String command) {
        BungeeMain plugin = BungeeMain.getInstance();
        plugin.discordBot.onAdminNotification(IDiscordListener.LogLevel.NORMAL, "Commande discord -> " + serverName + " : " + command);
        //EXECUTE BUNGEE
        if (serverName.equals("bungee")) {
            plugin.getProxy().getInstance().getPluginManager().dispatchCommand(plugin.getProxy().getConsole(), command);
        } else {
            ConsoleCommandRequest consoleCommandRequest = new ConsoleCommandRequest();
            consoleCommandRequest.command = command;
            MessageProxy messageProxy = new MessageProxy(consoleCommandRequest, ConsoleCommandRequest.class);
            ServerManager.INSTANCE.findServerProxy(serverName).request(messageProxy);
        }
    }
}
