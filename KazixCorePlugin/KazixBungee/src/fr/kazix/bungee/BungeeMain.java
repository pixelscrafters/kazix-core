package fr.kazix.bungee;

import fr.kazix.Utils;
import fr.kazix.bdd.BddManager;
import fr.kazix.bungee.listeners.PlayerListener;
import fr.kazix.bungee.logs.DiscordMessagesReceiver;
import fr.kazix.bungee.logs.LoggerAppender;
import fr.kazix.chat.Insults;
import fr.kazix.discord.DiscordBot;
import fr.kazix.discord.DiscordConfiguration;
import fr.kazix.discord.IDiscordListener;
import fr.kazix.files.JsonReader;
import fr.kazix.player.PlayerManager;
import fr.kazix.servers.ServerManager;
import net.md_5.bungee.api.plugin.Plugin;
import org.apache.logging.log4j.LogManager;

import javax.security.auth.login.LoginException;
import java.io.IOException;

public class BungeeMain extends Plugin {
    private static final org.apache.logging.log4j.core.Logger logger = (org.apache.logging.log4j.core.Logger) LogManager.getRootLogger();
    public static DiscordBot discordBot;

    private static BungeeMain instance = null;
    private static PlayerManager playerManager = new PlayerManager();
    private static Insults insults = new Insults();

    public static Insults getInsults() {
        return insults;
    }

    public static BungeeMain getInstance() {
        return instance;
    }

    public static PlayerManager getPlayerManager() {
        return playerManager;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;
        DiscordBot discordBot = new DiscordBot();
        //Activer le bot discord.

        try {
            discordBot.createBot(new JsonReader().read(DiscordConfiguration.class, "discord.json"));

            Utils.discordListener = discordBot;
            discordBot.onMessageSent(":white_check_mark: Le serveur est en cours de préchauffage !");
            discordBot.onAdminNotification(IDiscordListener.LogLevel.NORMAL, ":white_check_mark: Plugin KAZIX chargé ! Affichage de la console.");
            discordBot.registeredListener = new DiscordMessagesReceiver();
            LoggerAppender appender = new LoggerAppender(discordBot);

            logger.addAppender(appender);
            ServerManager.INSTANCE.bind(65200);
        } catch (LoginException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.discordBot = discordBot;
        getProxy().getPluginManager().registerListener(this, new PlayerListener());


        try {
            insults = new JsonReader().read(Insults.class, "insults.json");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex){
            discordBot.onAdminNotification(IDiscordListener.LogLevel.ERROR, "Configuration des insultes impossible à appliquer");
        }

        BddManager.INSTANCE.setup("pcn_accounts", "jdbc:mysql://localhost:3306/", "dModeratorManager", "vidvin");
        /*
        TODO : Initialiser les serveurs et charger les données
        - > Connexion SQL
        - > Chargement des bases
        - > Démarrer le serveur pour les sous serveurs
        - > Accepter les serveurs, les enregistrer
        - > ...
         */
    }

    @Override
    public void onDisable() {
        discordBot.onMessageSent(":bed: Le serveur est en train d'aller dormir !");
        discordBot.onAdminNotification(IDiscordListener.LogLevel.WARNING, ":bed: Arrêt du serveur.\nJoueurs pendant l'arrêt : "
                + getProxy().getPlayers().size());
        super.onDisable();
    }
}
