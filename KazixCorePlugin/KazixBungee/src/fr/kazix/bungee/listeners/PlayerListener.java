package fr.kazix.bungee.listeners;

import fr.kazix.bdd.BddManager;
import fr.kazix.bungee.BungeeMain;
import fr.kazix.discord.IDiscordListener;
import fr.kazix.player.KPlayer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlayerListener implements Listener {
    @EventHandler
    public void onLogin(PostLoginEvent event) {
        BungeeMain.discordBot.onPlayerJoin(event.getPlayer().getDisplayName());
        BungeeMain.discordBot.onAdminNotification(IDiscordListener.LogLevel.NORMAL, ":arrow_right: Connexion de " + event.getPlayer().getDisplayName()
                + " ip : " + event.getPlayer().getSocketAddress().toString());
        BungeeMain.getInstance().getProxy().broadcast(new TextComponent("§d☀ Connexion de : §l" + event.getPlayer().getDisplayName()));
        KPlayer player = new KPlayer(event.getPlayer().getUniqueId().toString(), event.getPlayer().getDisplayName(), "NOTHING", 10);
        BungeeMain.getInstance().getPlayerManager().addNewPlayer(player);
    }

    @EventHandler
    public void onChat(ChatEvent event) {
        if (!event.isCommand()) {
            BungeeMain.discordBot.onMessageSent(":rocket: **" + ((ProxiedPlayer) event.getSender()).getDisplayName() + "** : " + event.getMessage());
            KPlayer player = BungeeMain.getPlayerManager().getPlayer(((ProxiedPlayer) event.getSender()).getUniqueId().toString());
            if (player != null) {
                BungeeMain.getInstance().getProxy()
                        .broadcast(new TextComponent(player.formatChat(event.getMessage(), BungeeMain.getInstance().getInsults().insults)));
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerDisconnectEvent event) {
        BungeeMain.discordBot.onPlayerLeft(event.getPlayer().getDisplayName());
        BungeeMain.discordBot.onAdminNotification(IDiscordListener.LogLevel.NORMAL, ":arrow_left: Déconnexion de " + event.getPlayer().getDisplayName()
                + " ip : " + event.getPlayer().getSocketAddress().toString());
        BungeeMain.getInstance().getProxy().broadcast(new TextComponent("§d Déconnexion de : §l" + event.getPlayer().getDisplayName()));
    }

    @EventHandler
    public void onPlayerPreLogin(PreLoginEvent event) {
        String name = event.getConnection().getName();
        //String uuid = event.getConnection().getUniqueId().toString();
        String ip = event.getConnection().getSocketAddress().toString();
        ip = ip.substring(0, ip.indexOf(":"));

        try {
            BddManager.INSTANCE.login();
            // récup les IPS
            ResultSet rs = BddManager.INSTANCE.executeQuery(false, "SELECT * FROM players INNER JOIN ips ON players.id = ips.idPlayer WHERE playerName LIKE ? ;", name);
            boolean exists = false;
            boolean containsIp = false;
            while (rs.next()){
                exists = true;
                if(rs.getString("ip").equals(ip)){
                    containsIp = true;
                }
            }
            if(exists && !containsIp){
                // sinon si le joueur existe et que l'ip n'existe pas -> On ajoute son ip
                BddManager.INSTANCE.executeQuery(true, "INSERT INTO `ips` (`id`, `idPlayer`, `ip`) VALUES (NULL, (SELECT players.id FROM players WHERE playerName LIKE ?), ?);", name, ip);
            } else if(!exists){
                // si pas d'ip -> le joueur n'existe pas -> créer le joueur PUIS récuperer son ID et y associer son ip
                BddManager.INSTANCE.executeQuery(true,"INSERT INTO `players` (`id`, `playerName`, `etat`, `uuid`) VALUES (NULL, ?, 'NON_VERIFIE', NULL);", name);
                BddManager.INSTANCE.executeQuery(true,"INSERT INTO `ips` (`id`, `idPlayer`, `ip`) VALUES (NULL, (SELECT players.id FROM players WHERE playerName LIKE ?), ?);", name, ip);
            }

            //On fait la grosse requête
            ResultSet rs2 = BddManager.INSTANCE.executeQuery(false, "SELECT players.playerName, players.etat FROM ips INNER JOIN players ON ips.idPlayer = players.id WHERE ips.ip IN (SELECT ips.ip FROM players INNER JOIN ips ON ips.idPlayer = players.id WHERE\n" +
                    "players.playerName LIKE ?) GROUP BY players.playerName ;", name);
            List<String> names = new ArrayList<>();
            boolean isBanned = false;
            while(rs2.next()){
                names.add(rs2.getString("playerName"));
                if("COMPTE_DANGEREUX".equals(rs2.getString("etat"))){
                    isBanned = true;
                }
            }
            if(isBanned){
                event.setCancelled(true);
                event.setCancelReason(new TextComponent("Compte dangereux"));
            }
            BungeeMain.getInstance().getLogger().info("Le joueur " + name + " a les comptes suivants : " + String.join(", ", names));

            BddManager.INSTANCE.logout();
        } catch (SQLException throwables) {
            BungeeMain.getInstance().getProxy().getLogger().warning("Erreur de connexion à la BDD : " + throwables.getLocalizedMessage());
        } catch (ClassNotFoundException e) {
            BungeeMain.getInstance().getProxy().getLogger().warning("Dépendance mysql non trouvée : " + e.getLocalizedMessage());
        } finally {
            try {
                BddManager.INSTANCE.logout();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
